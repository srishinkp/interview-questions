# interview-questions


1. Create a polyfill for array map function
```js
function map(cb) {
  const newArray = [];
  this.forEach((item, index) => newArray.push(cb(item, index, this)));
  return newArray;
}
const result = map.call([2, 3, 4], (item) => item * 2);
```
2. Create a function that accepts an array and arranges the array in the below format 
    I/P = [12, 3, 4, 15, 1, 10]
    O/P = [15, 1, 12, 3, 10, 4]
```js
const formatArray = (arr = []) => {
  const sorted = arr.sort((a, b) => b - a);
  const length = sorted.length - 1;
  const result = [];
  for (let i = 0; i <= parseInt(length / 2); i++) {
    // sorted.splice(2*i+1, 0, sorted.pop());
    result.push(sorted.shift())
    result.push(sorted.pop())
  }
  console.log("result", result);
};
formatArray([12, 3, 4, 15, 1, 10]);
```
3. Create a function that returns nested array elements in same order to a one dimensional array
```js
const recrFun = (array = []) => {
  let elem = array.shift();
  let elvalue = [elem];
  if (elem && Array.isArray(elem) && elem.length) {
    elvalue = recrFun(elem);
  }
  if (array.length) {
    return [...elvalue, ...recrFun(array)];
  }
  return elvalue;
};
console.log(recrFun([1, [4, [5]], [1, 2, [3]], [4]]));
```
